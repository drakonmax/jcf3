import java.util.List;
import java.util.Objects;

public class Certificate {
    private String student;
    private String university;
    private String department;
    private String speciality;
    private String beginDate;
    private String endDate;
    private List<TableString> table;

    public Certificate(String student, String university, String department, String speciality,
                       String beginDate, String endDate, List<TableString> table) {
        this.student = student;
        this.university = university;
        this.department = department;
        this.speciality = speciality;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.table = table;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<TableString> getTable() {
        return table;
    }

    public void setTable(List<TableString> table) {
        this.table = table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Certificate that = (Certificate) o;
        return Objects.equals(student, that.student) &&
                Objects.equals(university, that.university) &&
                Objects.equals(department, that.department) &&
                Objects.equals(speciality, that.speciality) &&
                Objects.equals(beginDate, that.beginDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(table, that.table);
    }

    @Override
    public int hashCode() {
        return Objects.hash(student, university, department, speciality, beginDate, endDate, table);
    }
}
