public enum Mark {
    SET_OFF_FAIL(0, "Не зачтено"),
    SET_OFF_PASS(1, "Зачтено"),
    EXAM_FAIL(2, "Неудовлетворительно"),
    EXAM_PASS(3, "Удовлетворительно"),
    EXAM_PASS_GOOD(4, "Хорошо"),
    EXAM_PASS_EXCELLENT(5, "Отлично");


    private int value;
    private String name;


    Mark(int value, String name) {
        this.value = value;
        this.name = name;
    }


    public int getValue() {
        return value;
    }


    public String getName() {
        return name;
    }
}
