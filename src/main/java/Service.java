import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Service {

    public static List<String> getDisciplines(Certificate certificate){
        List<TableString> table = certificate.getTable();
        List<String> res = new ArrayList<>(table.size());
        for (TableString string: table){
            res.add(string.getDiscipline());
        }

        return res;
    }

    public static int getSumWorkload(Certificate certificate){
        int sum = 0;
        for (TableString string: certificate.getTable()){
            sum += string.getWorkload();
        }

        return sum;
    }

    public static double getMiddleMark(Certificate certificate) {
        int sum = 0;
        int quantity = 0;
        List<TableString> table = certificate.getTable();
        for (TableString string: table) {
            int mark = string.getMark().getValue();
            if (mark >= 3) {
                sum += string.getMark().getValue();
                quantity++;
            }
        }
        if (quantity == 0){
            return 0;
        }
        else
            return (double) sum / quantity;
    }

    public static Map<String, String> getDisciplineMarkMap(Certificate certificate) {
        Map<String, String> disciplineMarkMap = new HashMap<>();
        for (TableString string: certificate.getTable()){
            disciplineMarkMap.put(string.getDiscipline(), string.getMark().getName());
        }

        return disciplineMarkMap;
    }


    public static Map<String, List<String>> getMarkDisciplineMap(Certificate certificate) {
        Map<String, List<String>> markDisciplineMap = new HashMap<>(6);
        for (Mark mark: Mark.values()){
            markDisciplineMap.put(mark.getName(), new ArrayList<>());
        }
        for (TableString string: certificate.getTable()){
            markDisciplineMap.get(string.getMark().getName()).add(string.getDiscipline());
        }

        return markDisciplineMap;
    }

}
