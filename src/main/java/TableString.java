import java.util.Objects;

public class TableString {
    private String discipline;
    private int workload;
    private Mark mark;

    public TableString(String discipline, int workload, Mark mark) {
        setDiscipline(discipline);
        setWorkload(workload);
        setMark(mark);
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public int getWorkload() {
        return workload;
    }

    public void setWorkload(int workload) {
        this.workload = workload;
    }

    public Mark getMark() {
        return mark;
    }

    public void setMark(Mark mark) {
        this.mark = mark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TableString that = (TableString) o;
        return workload == that.workload &&
                Objects.equals(discipline, that.discipline) &&
                mark == that.mark;
    }

    @Override
    public int hashCode() {
        return Objects.hash(discipline, workload, mark);
    }
}
