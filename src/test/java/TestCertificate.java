import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

public class TestCertificate {
    @Test
    void testCertificate1() {
        List<TableString> table = new ArrayList<>(3);
        table.add(new TableString("Матанализ", 144, Mark.EXAM_PASS_GOOD));
        table.add(new TableString("Физкультура", 196, Mark.SET_OFF_FAIL));
        table.add(new TableString("С++", 64, Mark.SET_OFF_PASS));

        Certificate certificate = new Certificate("Москвичёв Максим Алексеевич", "ОмГУ",
                "ИМИТ", "Прикладная математика и информатика", "01.09.2018", "22.06.2022", table);

        assertEquals("Москвичёв Максим Алексеевич", certificate.getStudent());
        assertEquals("ОмГУ", certificate.getUniversity());
        assertEquals("ИМИТ", certificate.getDepartment());
        assertEquals("Прикладная математика и информатика", certificate.getSpeciality());
        assertEquals("01.09.2018", certificate.getBeginDate());
        assertEquals("22.06.2022", certificate.getEndDate());
        assertEquals(table, certificate.getTable());
    }
}
