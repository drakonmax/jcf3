import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
public class TestTableString {
    
    @Test
    void testTableStringExam() {
        TableString string = new TableString("Функциональный анализ", 128, Mark.EXAM_FAIL);

        assertEquals("Функциональный анализ", string.getDiscipline());
        assertEquals(128, string.getWorkload());
        assertEquals(Mark.EXAM_FAIL, string.getMark());
    }


    @Test
    void testTableStringSetOff() {
        TableString string = new TableString("С++", 144, Mark.SET_OFF_PASS);
        TableString string2 = new TableString("Физкультура", 196, Mark.SET_OFF_FAIL);

        assertEquals("С++", string.getDiscipline());
        assertEquals(144, string.getWorkload());
        assertEquals("Физкультура", string2.getDiscipline());
        assertEquals(196, string2.getWorkload());
        assertEquals(Mark.SET_OFF_PASS, string.getMark());
        assertEquals(Mark.SET_OFF_FAIL, string2.getMark());
    }


    @Test
    void testEquals() {
        TableString string1 = new TableString("ЯМПЫ", 258, Mark.EXAM_PASS);
        TableString string2 = new TableString("ЯМПЫ", 258, Mark.EXAM_PASS);
        TableString string3 = new TableString("Программирование на C++", 128, Mark.EXAM_PASS);
        TableString string4 = new TableString("ЯМПЫ", 200, Mark.EXAM_PASS);

        assertEquals(string1, string1);
        assertEquals(string1, string2);
        assertEquals(string2, string1);
        assertNotEquals(string1, "");
        assertNotEquals(string1, string3);
        assertNotEquals(string1, string4);
    }
}
